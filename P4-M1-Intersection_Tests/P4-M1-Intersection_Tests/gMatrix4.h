//Base code written by Jan Allbeck, Chris Czyzewicz, and Cory Boatright
//University of Pennsylvania
 
//modified March 2, 2017 at Grove City College

//Last Edited 4/20/17 by Samuel Kenney, Grove City College
 
#pragma once
 
#include "gVector4.h"
#include <cmath>

class gMatrix4{
private:
	gVector4 data[4];
	static const float PI_RAD;

public:
	///----------------------------------------------------------------------
	/// Constructors
	///----------------------------------------------------------------------
	/// Default Constructor.  Initialize to matrix of all 0s.
	gMatrix4(void);

	/// Initializes matrix with each vector representing a row in the matrix
	gMatrix4(const gVector4&, const gVector4&, const gVector4&, const gVector4&);

	/// Copy constructor
	gMatrix4(const gMatrix4&);

	///----------------------------------------------------------------------
	/// Getters
	///----------------------------------------------------------------------	
	/// Returns the values of the row at the index
	gVector4 operator[](unsigned int) const;
	
	/// Returns a reference to the row at the index
	gVector4& operator[](unsigned int);
	
	/// Returns a column of the matrix
	gVector4 getColumn(unsigned int) const;

	///----------------------------------------------------------------------
	/// Matrix Operations
	///----------------------------------------------------------------------	
	/// Returns the transpose of the matrix (v_ij == v_ji)
	gMatrix4 transpose(void) const;

	/// Returns inverted matrix for transforming a matrix to object space
	gMatrix4 invert(gMatrix4) const;

	/// Returns inverted matrix for transforming a vector to object space
	gMatrix4 invertstar(gMatrix4) const;

	/// Returns determinant of matrix a 3 x 3 matrix using a 4 x 4
	float area_from_three_points(gVector4,gVector4,gVector4) const;

	/// Returns determinant of matrix a 4 x 4 matrix
	float determinant4(gMatrix4) const;

	bool approxEquals(float a, float b)
	{
		const float epsilon = 0.00001f;
		if (a == b) {
			return true;
		}
		else if(a * b == 0.0f) {
			return fabs(a - b) < epsilon * epsilon;
		}
		else {
			return fabs(a - b) / (fabs(a) + fabs(b)) < epsilon;
		}
	}

	///----------------------------------------------------------------------
	/// Static Initializers
	///----------------------------------------------------------------------	
	/// Creates a 3-D rotation matrix.
	/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the x-axis
	static gMatrix4 rotateX(float);
	
	/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the y-axis
	static gMatrix4 rotateY(float);
	
	/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the z-axis
	static gMatrix4 rotateZ(float);

	/// Quaternion rotation
	//static gMatrix4 rotation();
	
	/// Takes an x, y, and z displacement and outputs a 4x4 translation matrix
	static gMatrix4 translate3D(float, float, float);
	
	/// Takes an x, y, and z scale and outputs a 4x4 scale matrix
	static gMatrix4 scale3D(float, float, float);
	
	/// Generates a 4x4 identity matrix
	static gMatrix4 identity(void);
	
	///----------------------------------------------------------------------
	/// Operator Overloading
	///----------------------------------------------------------------------
	/// Checks if m1 == m2
 	bool operator==(const gMatrix4&) const;
	
	/// Checks if m1 != m2
	bool operator!=(const gMatrix4&) const;
	
	/// Matrix addition (m1 + m2)
	gMatrix4 operator+(const gMatrix4&) const;

	/// Destructive matrix addition: remember to return *this
	gMatrix4& operator+=(const gMatrix4&);
	
	/// Matrix subtraction (m1 - m2)
	gMatrix4 operator-(const gMatrix4&) const;

	/// Destructive matrix subtraction: remember to return *this
	gMatrix4& operator-=(const gMatrix4&);
	
	/// Matrix multiplication (m1 * m2)
	gMatrix4 operator*(const gMatrix4&) const;

	/// Destructive matrix multiplication: remember to return *this
	gMatrix4& operator*(const gMatrix4&);
	
	/// Matrix/vector multiplication (m * v)
	/// Assume v is a column vector (ie. a 4x1 matrix)
	gVector4 operator*(const gVector4&) const;
};