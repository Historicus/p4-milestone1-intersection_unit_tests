#include "gMatrix4.h"
#include <cmath>

const float gMatrix4::PI_RAD = 0.01745329251994329576923690768489f;

gMatrix4::gMatrix4() {
	data[0] = gVector4(0.0f, 0.0f, 0.0f, 0.0f);
	data[1] = gVector4(0.0f, 0.0f, 0.0f, 0.0f);
	data[2] = gVector4(0.0f, 0.0f, 0.0f, 0.0f);
	data[3] = gVector4(0.0f, 0.0f, 0.0f, 0.0f);
}

gMatrix4::gMatrix4(const gVector4& row0, const gVector4& row1, const gVector4& row2, const gVector4& row3) {
	data[0] = row0;
	data[1] = row1;
	data[2] = row2;
	data[3] = row3;
}

gMatrix4::gMatrix4(const gMatrix4& other) {
	data[0] = other.data[0];
	data[1] = other.data[1];
	data[2] = other.data[2];
	data[3] = other.data[3];
}

gVector4 gMatrix4::operator[](unsigned int index) const {
	return data[index];
}

gVector4& gMatrix4::operator[](unsigned int index) {
	return data[index];
}

gVector4 gMatrix4::getColumn(unsigned int index) const {
	return gVector4(data[0][index], data[1][index], data[2][index], data[3][index]);
}

gMatrix4 gMatrix4::transpose() const {
	return gMatrix4(getColumn(0), getColumn(1), getColumn(2), getColumn(3));
}

/// Returns inverted matrix for transforming a matrix to object space
gMatrix4 gMatrix4::invert(gMatrix4 C) const {
	float detC = determinant4(C);
	gMatrix4 invertC;
	
	//test if not equal to 0.0f
	if (!invertC.approxEquals(detC, 0.0f)){
		invertC[0][0] = C[1][1] * C[2][2] * C[3][3] + C[1][2] * C[2][3] * C[3][2] + C[1][2] * C[2][3] * C[3][2] - C[1][1] * C[2][3] * C[3][2] - C[1][2] * C[2][1] * C[3][3] - C[1][3] * C[2][2] * C[3][1];
		invertC[0][1] = C[0][1] * C[2][3] * C[3][2] + C[0][2] * C[2][1] * C[3][3] + C[0][3] * C[2][2] * C[3][1] - C[0][1] * C[2][2] * C[3][3] - C[0][2] * C[2][3] * C[3][1] - C[0][3] * C[2][1] * C[3][2];
		invertC[0][2] = C[0][1] * C[1][2] * C[3][3] + C[0][2] * C[1][3] * C[3][1] + C[0][3] * C[1][1] * C[3][2] - C[0][1] * C[1][3] * C[3][2] - C[0][2] * C[1][1] * C[3][3] - C[0][3] * C[1][2] * C[3][1];
		invertC[0][3] = C[0][1] * C[1][3] * C[2][2] + C[0][2] * C[1][1] * C[2][3] + C[0][3] * C[1][2] * C[2][1] - C[0][1] * C[1][2] * C[2][3] - C[0][2] * C[1][3] * C[2][1] - C[0][3] * C[1][1] * C[2][2];					
		invertC[1][0] = C[1][0] * C[2][3] * C[3][2] + C[1][2] * C[2][0] * C[3][3] + C[1][3] * C[2][2] * C[3][0] - C[1][0] * C[2][2] * C[3][3] - C[1][2] * C[2][3] * C[3][0] - C[1][3] * C[2][0] * C[3][2];
		invertC[1][1] = C[0][0] * C[2][2] * C[3][3] + C[0][2] * C[2][3] * C[3][0] + C[0][3] * C[2][0] * C[3][2] - C[0][0] * C[2][3] * C[3][2] - C[0][2] * C[2][0] * C[3][3] - C[0][3] * C[2][2] * C[3][0];
		invertC[1][2] = C[0][0] * C[1][3] * C[3][2] + C[0][2] * C[1][0] * C[3][3] + C[0][3] * C[1][2] * C[3][0] - C[0][0] * C[1][2] * C[3][3] - C[0][2] * C[1][3] * C[3][0] - C[0][3] * C[1][0] * C[3][2];
		invertC[1][3] = C[0][0] * C[1][2] * C[2][3] + C[0][2] * C[1][3] * C[2][0] + C[0][3] * C[1][0] * C[2][2] - C[0][0] * C[1][3] * C[2][2] - C[0][2] * C[1][0] * C[2][3] - C[0][3] * C[1][2] * C[2][0];
		invertC[2][0] = C[1][0] * C[2][1] * C[3][3] + C[1][1] * C[2][3] * C[3][0] + C[1][3] * C[2][0] * C[3][1] - C[1][0] * C[2][3] * C[3][1] - C[1][1] * C[2][0] * C[3][3] - C[1][3] * C[2][1] * C[3][0];
		invertC[2][1] = C[0][0] * C[2][3] * C[3][1] + C[0][1] * C[2][0] * C[3][3] + C[0][3] * C[2][1] * C[3][0] - C[0][0] * C[2][1] * C[3][3] - C[0][1] * C[2][3] * C[3][0] - C[0][3] * C[2][0] * C[3][1];
		invertC[2][2] = C[0][0] * C[1][1] * C[3][3] + C[0][1] * C[1][3] * C[3][0] + C[0][3] * C[1][0] * C[3][1] - C[0][0] * C[1][3] * C[3][1] - C[0][1] * C[1][0] * C[3][3] - C[0][3] * C[1][1] * C[3][0];
		invertC[2][3] = C[0][0] * C[1][3] * C[2][1] + C[0][1] * C[1][0] * C[2][3] + C[0][3] * C[1][1] * C[2][0] - C[0][0] * C[1][1] * C[2][3] - C[0][1] * C[1][3] * C[2][0] - C[0][3] * C[1][0] * C[2][1];
		invertC[3][0] = C[1][0] * C[2][2] * C[3][1] + C[1][1] * C[2][0] * C[3][2] + C[1][2] * C[2][1] * C[3][0] - C[1][0] * C[2][1] * C[3][2] - C[1][1] * C[2][2] * C[3][0] - C[1][2] * C[2][0] * C[3][1];
		invertC[3][1] = C[0][0] * C[2][1] * C[3][2] + C[0][1] * C[2][2] * C[3][0] + C[0][2] * C[2][0] * C[3][1] - C[0][0] * C[2][2] * C[3][1] - C[0][1] * C[2][0] * C[3][2] - C[0][2] * C[2][1] * C[3][0];
		invertC[3][2] = C[0][0] * C[1][2] * C[3][1] + C[0][1] * C[1][0] * C[3][2] + C[0][2] * C[1][1] * C[3][0] - C[0][0] * C[1][1] * C[3][2] - C[0][1] * C[1][2] * C[3][0] - C[0][2] * C[1][0] * C[3][1];
		invertC[3][3] = C[0][0] * C[1][1] * C[2][2] + C[0][1] * C[1][2] * C[2][0] + C[0][2] * C[1][0] * C[2][1] - C[0][0] * C[1][2] * C[2][1] - C[0][1] * C[1][0] * C[2][2] - C[0][2] * C[1][1] * C[2][0];
	}

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			invertC[i][j] = invertC[i][j] * (1/detC);
		}
	}

	return invertC;
}

/// Returns determinant of matrix a 3 x 3 matrix using a 4 x 4
float gMatrix4::area_from_three_points(gVector4 p1,gVector4 p2,gVector4 p3) const{
	
	//calculate determinant
	float a00 = p1[1], a01 = p1[2], a02 = 1.0f;
	float a10 = p2[1], a11 = p2[2], a12 = 1.0f;
	float a20 = p3[1], a21 = p3[2], a22 = 1.0f;

	float b00 = p1[2], b01 = p1[0], b02 = 1.0f;
	float b10 = p2[2], b11 = p2[0], b12 = 1.0f;
	float b20 = p3[2], b21 = p3[0], b22 = 1.0f;

	float c00 = p1[0], c01 = p1[1], c02 = 1.0f;
	float c10 = p2[0], c11 = p2[1], c12 = 1.0f;
	float c20 = p3[0], c21 = p3[1], c22 = 1.0f;

	float detA = (a00 * (a11 * a22 - a12 * a21) - a01 * (a10 * a22 - a12 * a20) + a02 * (a10 * a21 - a11 * a20));
	float detB = (b00 * (b11 * b22 - b12 * b21) - b01 * (b10 * b22 - b12 * b20) + b02 * (b10 * b21 - b11 * b20));
	float detC = (c00 * (c11 * c22 - c12 * c21) - c01 * (c10 * c22 - c12 * c20) + c02 * (c10 * c21 - c11 * c20));

	float foo = (detA * detA) + (detB * detB) + (detC * detC);
	float area =  0.5f * (sqrt((detA * detA) + (detB * detB) + (detC * detC)));
	return area;
}

/// Returns inverted matrix for transforming a vector to object space
gMatrix4 gMatrix4::invertstar(gMatrix4 C) const {
	float detC = determinant4(C);
	//makes the [0][0],[0][1],[0][2] 0.0f
	for (int i = 0; i < 3; i++)
	{
		C[i][3] = 0.0f;
	}

	gMatrix4 invertC;

	//test if not equal to 0.0f
	if (!invertC.approxEquals(detC, 0.0f)){
		invertC[0][0] = C[1][1] * C[2][2] * C[3][3] + C[1][2] * C[2][3] * C[3][2] + C[1][2] * C[2][3] * C[3][2] - C[1][1] * C[2][3] * C[3][2] - C[1][2] * C[2][1] * C[3][3] - C[1][3] * C[2][2] * C[3][1];
		invertC[0][1] = C[0][1] * C[2][3] * C[3][2] + C[0][2] * C[2][1] * C[3][3] + C[0][3] * C[2][2] * C[3][1] - C[0][1] * C[2][2] * C[3][3] - C[0][2] * C[2][3] * C[3][1] - C[0][3] * C[2][1] * C[3][2];
		invertC[0][2] = C[0][1] * C[1][2] * C[3][3] + C[0][2] * C[1][3] * C[3][1] + C[0][3] * C[1][1] * C[3][2] - C[0][1] * C[1][3] * C[3][2] - C[0][2] * C[1][1] * C[3][3] - C[0][3] * C[1][2] * C[3][1];
		invertC[0][3] = C[0][1] * C[1][3] * C[2][2] + C[0][2] * C[1][1] * C[2][3] + C[0][3] * C[1][2] * C[2][1] - C[0][1] * C[1][2] * C[2][3] - C[0][2] * C[1][3] * C[2][1] - C[0][3] * C[1][1] * C[2][2];					
		invertC[1][0] = C[1][0] * C[2][3] * C[3][2] + C[1][2] * C[2][0] * C[3][3] + C[1][3] * C[2][2] * C[3][0] - C[1][0] * C[2][2] * C[3][3] - C[1][2] * C[2][3] * C[3][0] - C[1][3] * C[2][0] * C[3][2];
		invertC[1][1] = C[0][0] * C[2][2] * C[3][3] + C[0][2] * C[2][3] * C[3][0] + C[0][3] * C[2][0] * C[3][2] - C[0][0] * C[2][3] * C[3][2] - C[0][2] * C[2][0] * C[3][3] - C[0][3] * C[2][2] * C[3][0];
		invertC[1][2] = C[0][0] * C[1][3] * C[3][2] + C[0][2] * C[1][0] * C[3][3] + C[0][3] * C[1][2] * C[3][0] - C[0][0] * C[1][2] * C[3][3] - C[0][2] * C[1][3] * C[3][0] - C[0][3] * C[1][0] * C[3][2];
		invertC[1][3] = C[0][0] * C[1][2] * C[2][3] + C[0][2] * C[1][3] * C[2][0] + C[0][3] * C[1][0] * C[2][2] - C[0][0] * C[1][3] * C[2][2] - C[0][2] * C[1][0] * C[2][3] - C[0][3] * C[1][2] * C[2][0];
		invertC[2][0] = C[1][0] * C[2][1] * C[3][3] + C[1][1] * C[2][3] * C[3][0] + C[1][3] * C[2][0] * C[3][1] - C[1][0] * C[2][3] * C[3][1] - C[1][1] * C[2][0] * C[3][3] - C[1][3] * C[2][1] * C[3][0];
		invertC[2][1] = C[0][0] * C[2][3] * C[3][1] + C[0][1] * C[2][0] * C[3][3] + C[0][3] * C[2][1] * C[3][0] - C[0][0] * C[2][1] * C[3][3] - C[0][1] * C[2][3] * C[3][0] - C[0][3] * C[2][0] * C[3][1];
		invertC[2][2] = C[0][0] * C[1][1] * C[3][3] + C[0][1] * C[1][3] * C[3][0] + C[0][3] * C[1][0] * C[3][1] - C[0][0] * C[1][3] * C[3][1] - C[0][1] * C[1][0] * C[3][3] - C[0][3] * C[1][1] * C[3][0];
		invertC[2][3] = C[0][0] * C[1][3] * C[2][1] + C[0][1] * C[1][0] * C[2][3] + C[0][3] * C[1][1] * C[2][0] - C[0][0] * C[1][1] * C[2][3] - C[0][1] * C[1][3] * C[2][0] - C[0][3] * C[1][0] * C[2][1];
		invertC[3][0] = C[1][0] * C[2][2] * C[3][1] + C[1][1] * C[2][0] * C[3][2] + C[1][2] * C[2][1] * C[3][0] - C[1][0] * C[2][1] * C[3][2] - C[1][1] * C[2][2] * C[3][0] - C[1][2] * C[2][0] * C[3][1];
		invertC[3][1] = C[0][0] * C[2][1] * C[3][2] + C[0][1] * C[2][2] * C[3][0] + C[0][2] * C[2][0] * C[3][1] - C[0][0] * C[2][2] * C[3][1] - C[0][1] * C[2][0] * C[3][2] - C[0][2] * C[2][1] * C[3][0];
		invertC[3][2] = C[0][0] * C[1][2] * C[3][1] + C[0][1] * C[1][0] * C[3][2] + C[0][2] * C[1][1] * C[3][0] - C[0][0] * C[1][1] * C[3][2] - C[0][1] * C[1][2] * C[3][0] - C[0][2] * C[1][0] * C[3][1];
		invertC[3][3] = C[0][0] * C[1][1] * C[2][2] + C[0][1] * C[1][2] * C[2][0] + C[0][2] * C[1][0] * C[2][1] - C[0][0] * C[1][2] * C[2][1] - C[0][1] * C[1][0] * C[2][2] - C[0][2] * C[1][1] * C[2][0];
	}

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			invertC[i][j] = invertC[i][j] * (1/detC);
		}
	}

	return invertC;
}

/// Returns determinant of matrix
float gMatrix4::determinant4(gMatrix4 C) const {
	float detC = C[0][0] * C[1][1] * C[2][2] * C[3][3] + C[0][1] * C[1][2] * C[2][3] * C[3][1] + C[0][0] * C[1][3] * C[2][1] * C[3][2] +
				 C[0][1] * C[1][0] * C[2][3] * C[3][2] + C[0][1] * C[1][2] * C[2][0] * C[3][3] + C[0][1] * C[1][3] * C[2][2] * C[3][0] +
				 C[0][2] * C[1][0] * C[2][1] * C[3][3] + C[0][2] * C[1][1] * C[2][3] * C[3][0] + C[0][2] * C[1][3] * C[2][0] * C[3][1] +
				 C[0][3] * C[1][0] * C[2][2] * C[3][1] + C[0][3] * C[1][1] * C[2][0] * C[3][2] + C[0][3] * C[1][2] * C[2][1] * C[3][0] -

				 C[0][0] * C[1][1] * C[2][3] * C[3][2] - C[0][0] * C[1][2] * C[2][1] * C[3][3] - C[0][0] * C[1][3] * C[2][2] * C[3][1] -
				 C[0][1] * C[1][0] * C[2][2] * C[3][3] - C[0][1] * C[1][2] * C[2][3] * C[3][0] - C[0][1] * C[1][3] * C[2][0] * C[3][2] -
				 C[0][2] * C[1][0] * C[2][3] * C[3][1] - C[0][2] * C[1][1] * C[2][0] * C[3][3] - C[0][2] * C[1][3] * C[2][1] * C[3][0] -
				 C[0][3] * C[1][0] * C[2][1] * C[3][2] - C[0][3] * C[1][1] * C[2][2] * C[3][0] - C[0][3] * C[1][2] * C[2][0] * C[3][1];

	//test if not equal to 0.0f
	if (!C.approxEquals(detC, 0.0f))
		return detC;
	else 
		return 0.0f;
}


gMatrix4 gMatrix4::rotateX(float angle) {
	angle *= PI_RAD;
	float c = cosf(angle);
	float s = sinf(angle);
	return gMatrix4(gVector4(1.0f, 0.0f, 0.0f, 0.0f),
					gVector4(0.0f, c, -s, 0.0f),
					gVector4(0.0f, s, c, 0.0f),
					gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

gMatrix4 gMatrix4::rotateY(float angle) {
	angle *= PI_RAD;
	float c = cosf(angle);
	float s = sinf(angle);
	return gMatrix4(gVector4(c, 0.0f, s, 0.0f),
					gVector4(0.0f, 1.0f, 0.0f, 0.0f),
					gVector4(-s, 0.0f, c, 0.0f),
					gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

gMatrix4 gMatrix4::rotateZ(float angle) {
	angle *= PI_RAD;
	float c = cosf(angle);
	float s = sinf(angle);
	return gMatrix4(gVector4(c, -s, 0.0f, 0.0f),
					gVector4(s, c, 0.0f, 0.0f),
					gVector4(0.0f, 0.0f, 1.0f, 0.0f),
					gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

gMatrix4 gMatrix4::translate3D(float x, float y, float z) {
	return gMatrix4(gVector4(1.0f, 0.0f, 0.0f, x),
					gVector4(0.0f, 1.0f, 0.0f, y),
					gVector4(0.0f, 0.0f, 1.0f, z),
					gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

gMatrix4 gMatrix4::scale3D(float x, float y, float z) {
	return gMatrix4(gVector4(x, 0.0f, 0.0f, 0.0f),
					gVector4(0.0f, y, 0.0f, 0.0f),
					gVector4(0.0f, 0.0f, z, 0.0f),
					gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

gMatrix4 gMatrix4::identity() {
	return gMatrix4(gVector4(1.0f, 0.0f, 0.0f, 0.0f),
					gVector4(0.0f, 1.0f, 0.0f, 0.0f),
					gVector4(0.0f, 0.0f, 1.0f, 0.0f),
					gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

bool gMatrix4::operator==(const gMatrix4& m2) const {
	return data[0] == m2.data[0] && data[1] == m2.data[2] && data[2] == m2.data[2] && data[3] == m2.data[3];
}

bool gMatrix4::operator!=(const gMatrix4& m2) const {
	return !(*this == m2);
}

gMatrix4 gMatrix4::operator+(const gMatrix4& m2) const {
	return gMatrix4(data[0] + m2.data[0], data[1] + m2.data[1], data[2] + m2.data[2], data[3] + m2.data[3]);
}

gMatrix4& gMatrix4::operator+=(const gMatrix4& m2) {
	data[0] += m2.data[0];
	data[1] += m2.data[1];
	data[2] += m2.data[2];
	data[3] += m2.data[3];
	return *this;
}

gMatrix4 gMatrix4::operator-(const gMatrix4& m2) const {
	return gMatrix4(data[0] - m2.data[0], data[1] - m2.data[1], data[2] - m2.data[2], data[3] - m2.data[3]);
}

gMatrix4 & gMatrix4::operator-=(const gMatrix4& m2) {
	data[0] -= m2.data[0];
	data[1] -= m2.data[1];
	data[2] -= m2.data[2];
	data[3] -= m2.data[3];
	return *this;
}

gMatrix4 gMatrix4::operator*(const gMatrix4& m2) const {
	gMatrix4 other = m2.transpose();	//gets the columns, which can be indexed with regular operator[]
	gMatrix4 result(*this * other[0], *this * other[1], *this * other[2], *this * other[3]);
	return result.transpose();
}

gMatrix4& gMatrix4::operator*(const gMatrix4& m2) {
	gMatrix4 other = m2.transpose();
	gMatrix4 result(*this * other[0], *this * other[1], *this * other[2], *this * other[3]);
	data[0] = result.getColumn(0);
	data[1] = result.getColumn(1);
	data[2] = result.getColumn(2);
	data[3] = result.getColumn(3);
	return *this;
}

gVector4 gMatrix4::operator*(const gVector4& v) const {
	return gVector4(data[0] * v, data[1] * v, data[2] * v, data[3] * v);
}