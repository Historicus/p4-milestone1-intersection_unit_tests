/**
  This testing framework has been developed/overhauled over time, primarily by:
  Chris Czyzewicz
  Ben Sunshine-Hill
  Cory Boatright 
  
  While the three were PhD students at UPenn (through 2013).  This particular version has some
  modifications by Cory since joining the faculty of Grove City College.
  
  Last revised 4/10/2017
*/

#include "tests.h"
#include "stubs.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>

void RunRaySphereTests();
void RunRayPolyTests();
void RunRayCubeTests();
void RunYourTests();
void RunGradingTests();

typedef bool (*TestFunc)();

int g_numTests = 0;
int g_numSuccessful = 0;

void ReportTest(std::string name, bool result);

template<typename T>
void RunTest(std::string name, T const& testValue, T const& expectedValue) {
	ReportTest(name, testValue == expectedValue);
}

template<>
void RunTest<double>(std::string name, double const& testValue, double const& expectedValue) {
	ReportTest(name, (std::abs(testValue-expectedValue) / std::abs(expectedValue)) < 1e-3);
}

void RunTests() {
	std::cout.sync_with_stdio(true);

	RunRaySphereTests();
	RunRayPolyTests();
	RunRayCubeTests();
	RunYourTests();
	RunGradingTests();

	std::cout << g_numSuccessful << " of " << g_numTests << " tests successful. ";
	if(g_numTests == g_numSuccessful) {
		std::cout << "A winner is you!";
	}
	std::cout << std::endl;
}

const double SQRT_HALF = 0.70710678; // square root of one half
const double SQRT_TWO = 1.41421356; // figure it out

const gMatrix4 IDENTITY_MATRIX (gVector4(1.0f, 0.0f, 0.0f, 0.0f),
                         gVector4(0.0f, 1.0f, 0.0f, 0.0f),
                         gVector4(0.0f, 0.0f, 1.0f, 0.0f),
                         gVector4(0.0f, 0.0f, 0.0f, 1.0f));
const gMatrix4 DOUBLE_MATRIX(gVector4(2.0f, 0.0f, 0.0f, 0.0f),
                         gVector4(0.0f, 2.0f, 0.0f, 0.0f),
                         gVector4(0.0f, 0.0f, 2.0f, 0.0f),
                         gVector4(0.0f, 0.0f, 0.0f, 1.0f));
const gMatrix4 TALLANDSKINNY_MATRIX(gVector4(0.5f, 0.0f, 0.0f, 0.0f),
								gVector4(0.0f, 2.0f, 0.0f, 0.0f),
								gVector4(0.0f, 0.0f, 0.5f, 0.0f),
								gVector4(0.0f, 0.0f, 0.0f, 1.0f));
const gMatrix4 BACK5_MATRIX(gVector4(1.0f, 0.0f, 0.0f, 0.0f),
                        gVector4(0.0f, 1.0f, 0.0f, 0.0f),
                        gVector4(0.0f, 0.0f, 1.0f, 0.0f),
                        gVector4(0.0f, 0.0f, -5.0f, 1.0f));
const gMatrix4 BACK5ANDTURN_MATRIX(gVector4(SQRT_HALF, 0.0f, -SQRT_HALF, 0.0f),
                                  gVector4(0.0f, 1.0f, 0.0f, 0.0f),
                                  gVector4(SQRT_HALF, 0.0f, SQRT_HALF, 0.0f),
                                  gVector4(0.0f, 0.0f, -5.0f, 1.0f));


const gVector4 ZERO_VECTOR(0.0f, 0.0f, 0.0f, 0.0f);
const gVector4 ZERO_POINT(0.0f, 0.0f, 0.0f, 1.0f);
const gVector4 HALFX_VECTOR(0.5f, 0.0f, 0.0f, 0.0f);
const gVector4 HALFX_POINT(0.5f, 0.0f, 0.0f, 1.0f);
const gVector4 THIRDX_VECTOR(0.33333333333333333f, 0.0f, 0.0f, 0.0f);
const gVector4 THIRDX_POINT(0.33333333333333333f, 0.0f, 0.0f, 1.0f);
const gVector4 NEGX_VECTOR(-1.0f, 0.0f, 0.0f, 0.0f);
const gVector4 NEGX_POINT(-1.0f, 0.0f, 0.0f, 1.0f);
const gVector4 NEGZ_VECTOR(0.0f, 0.0f, -1.0f, 0.0f);
const gVector4 NEGY_VECTOR(0.0f, -1.0f, 0.0f, 0.0f);
const gVector4 POSZ_VECTOR(0.0f, 0.0f, 1.0f, 0.0f);
const gVector4 POSZ_POINT(0.0f, 0.0f, 1.0f, 1.0f);
const gVector4 POSXPOSZ_VECTOR(1.0f, 0.0f, 1.0f, 0.0f);
const gVector4 POSXPOSZ_POINT(1.0f, 0.0f, 1.0f, 1.0f);
const gVector4 ZNEGTEN_VECTOR(0.0f, 0.0f, -10.0f, 0.0f);
const gVector4 ZNEGTEN_POINT(0.0f, 0.0f, -10.0f, 1.0f);
const gVector4 ZPOSTEN_VECTOR(0.0f, 0.0f, 10.0f, 0.0f);
const gVector4 ZPOSTEN_POINT(0.0f, 0.0f, 10.0f, 1.0f);
const gVector4 YPOSTEN_VECTOR(0.0f, 10.0f, 0.0f, 0.0f);
const gVector4 XPOSTEN_VECTOR(10.0f, 0.0f, 0.0f, 0.0f);
const gVector4 POSXNEGZ_NORM_VECTOR(SQRT_HALF, 0.0f, -SQRT_HALF, 0.0f);
const gVector4 NEGFIVEOFIVE_VECTOR(-5.0f, 0.0f, 5.0f, 0.0f);
const gVector4 NEGFIVEOFIVE_POINT(-5.0f, 0.0f, 5.0f, 1.0f);

const gVector4 POINT_N1N10(-1.0f, -1.0f, 0.0f, 1.0f);
const gVector4 POINT_1N10(1.0f, -1.0f, 0.0f, 1.0f);
const gVector4 POINT_010(0.0f, 1.0f, 0.0f, 1.0f);
const gVector4 POINT_N2N10(-2.0f, -1.0f, 0.0f, 1.0f);
const gVector4 POINT_2N10(2.0f, -1.0f, 0.0f, 1.0f);

const double TEN_KAZILLION = 1e26;

void RunRaySphereTests() {
	RunTest(
		"Easy sphere",
		Test_RaySphereIntersect(ZERO_POINT, NEGZ_VECTOR, BACK5_MATRIX),
		4.0);

	RunTest(
		"Offset a bit",
		Test_RaySphereIntersect(HALFX_POINT, NEGZ_VECTOR, BACK5_MATRIX),
		4.13397);

	RunTest(
		"What sphere",
		Test_RaySphereIntersect(ZNEGTEN_POINT, NEGZ_VECTOR, BACK5_MATRIX),
		-1.0);

	RunTest(
		"Looking back",
		Test_RaySphereIntersect(ZNEGTEN_POINT, POSZ_VECTOR, BACK5_MATRIX),
		4.0);

	RunTest(
		"West pole",
		Test_RaySphereIntersect(ZERO_POINT, NEGZ_VECTOR, BACK5ANDTURN_MATRIX),
		4.0);

	RunTest(
		"Another angle",
		Test_RaySphereIntersect(NEGFIVEOFIVE_POINT, POSXNEGZ_NORM_VECTOR, IDENTITY_MATRIX),
		(5.0 * SQRT_TWO) - 1);
}

void RunRayPolyTests() {
	RunTest(
		"Hi, Tri",
		Test_RayPolyIntersect(POSZ_POINT, NEGZ_VECTOR, POINT_N1N10, POINT_1N10, POINT_010, IDENTITY_MATRIX),
		1.0);

	RunTest(
		"Bye, Tri",
		Test_RayPolyIntersect(POSXPOSZ_POINT, NEGZ_VECTOR, POINT_N1N10, POINT_1N10, POINT_010, IDENTITY_MATRIX),
		-1.0);

	RunTest(
		"Looking back",
		Test_RayPolyIntersect(POSZ_POINT, POSZ_VECTOR, POINT_N1N10, POINT_1N10, POINT_010, IDENTITY_MATRIX),
		-1.0);

	RunTest(
		"Flip it good",
		Test_RayPolyIntersect(POSZ_POINT, NEGZ_VECTOR, POINT_010, POINT_1N10, POINT_N1N10, IDENTITY_MATRIX),
		1.0);

	RunTest(
		"It moves",
		Test_RayPolyIntersect(ZERO_POINT, NEGZ_VECTOR, POINT_N1N10, POINT_1N10, POINT_010, BACK5ANDTURN_MATRIX),
		5.0);

	RunTest(
		"And turns",
		Test_RayPolyIntersect(HALFX_POINT, NEGZ_VECTOR, POINT_N2N10, POINT_2N10, POINT_010, BACK5ANDTURN_MATRIX),
		5.5);
}

void RunRayCubeTests() {
	RunTest(
		"Behold the cube",
		Test_RayCubeIntersect(ZERO_POINT, NEGZ_VECTOR, BACK5_MATRIX),
		4.5);

	RunTest(
		"The cube abides",
		Test_RayCubeIntersect(THIRDX_POINT, NEGZ_VECTOR, BACK5_MATRIX),
		4.5);

	RunTest(
		"Cuuuube!",
		Test_RayCubeIntersect(NEGX_POINT, NEGZ_VECTOR, BACK5_MATRIX),
		-1.0);

	RunTest(
		"Looking sharp, edge",
		Test_RayCubeIntersect(ZERO_POINT, NEGZ_VECTOR, BACK5ANDTURN_MATRIX),
		5.0 - SQRT_HALF);

	RunTest(
		"Big cube",
		Test_RayCubeIntersect(ZPOSTEN_POINT, NEGZ_VECTOR, DOUBLE_MATRIX),
		9.0);
	RunTest(
		"Strafing the cube",
		Test_RayCubeIntersect(NEGFIVEOFIVE_POINT, POSXNEGZ_NORM_VECTOR, IDENTITY_MATRIX),
		6.36);
}

void RunYourTests() {
	// It can be very useful to put tests of your own here. The unit tests above do NOT test everything!
}

void RunGradingTests() {
	// Leave this function alone; we'll put stuff in it for grading.
}

void ReportTest(std::string name, bool result) {
	std::cout << std::setfill('.') << std::setw(50) << std::left << name;
	std::cout << (result ? "SUCCESS" : "**FAILURE**") << std::endl;
	g_numTests++;
	if(result) {
		g_numSuccessful++;
	}
	else {
		// It can be very useful to put a breakpoint here
	}

}